dotfiles
========

Some notes on how to reproduce my notebook's configuration.

About this repository
---------------------

This repository has two folders:

 * `nixos/`   has NixOS' system configuration.
 * `homedir/` has user's dotfiles.

NixOS Instalation on a Thinkpad X1 Carbon 4th Generation
--------------------------------------------------------

I strongly recommended you get acquainted with NixOS in a virtual machine
before trying to install it in a *"real machine"*.

The following commands where tested using *NixOS Unstable's Minimal Installer*
**nixos-minimal-17.03pre98682.f673243-x86_64-linux.iso**. You can get the
latest install image from [this site](https://nixos.org/channels/), in the
**nixos-unstable** folder.

### Getting WiFi access

NixOS needs Internet access to download a few packages during installation.
If you use an Ethernet network with DHCP, everything is probably already
working.

If you really need to use WiFi, first we have to discover the interface name:

```
# iw dev
```

Assuming your wifi device name is **wlp4s0**, your SSID is **MyNetwork**,
your password is **12345** and you are using *WPA*, you can connect using this:

```
systemctl stop wpa_supplicant
wpa_supplicant -i wlp4s0 -c <(wpa_passphrase 'MyNetwork' '12345')
```

Now press `Alt + F2` to go to another terminal and continue the installation.

### Preparing the disk

To setup an encrypted LVM machine we would, ideally, fill the entire device
with random data. To write crypto-grade pseudorandom data, type the following
commands (vide question 2.19 in [this FAQ](https://gitlab.com/cryptsetup/cryptsetup/wikis/FrequentlyAskedQuestions#2-setup))
assumming your device is `/dev/sda`:

```
# cryptsetup open --type plain --key-file /dev/random /dev/sda foo
# dd if=/dev/zero of=/dev/mapper/foo bs=1M status=progress
# cryptsetup luksClose foo
```
It will probably take a few hours to overwrite a big disk. So, if you are
using a virtual machine or are not a very security-concious person, you should
probably skip these commands.

### Partitioning

We need to create a small `/boot` partition (1GiB is more than enough)
and use the rest of the disk for the encrypted LVM:

```
# parted /dev/sda
(parted) mklabel msdos
(parted) mkpart primary 1MiB 1GiB
(parted) mkpart primary 1GiB -0
(parted) set 2 lvm on
(parted) q
```

This leaves us with two partitions:
 - `/dev/sda1` will be mounted later on `/boot`.
 - `/dev/sda2` will be used as an encrypted container.

### Setting the encrypted LVM

Now we create a LVM inside the encrypted partition, containing a volume
group named `nixos-vg`, containing two logical volumes: `swap`, taking 16GiB,
and `root`, taking all the free space left.

```
# cryptsetup luksFormat /dev/sda2
# cryptsetup luksOpen /dev/sda2 lvm
# pvcreate /dev/mapper/lvm
# vgcreate nixos-vg /dev/mapper/lvm
# lvcreate -L 16G -n swap nixos-vg
# lvcreate -l 100%FREE -n root nixos-vg
```

After these commands, these devices are ready to be used:
 - `/dev/nixos-vg/swap` is our swap partition.
 - `/dev/nixos-vg/root` will be mounted as the root filesystem.

### Creating the filesystem and swap

We initialize the filesystems and swap:

```
# mkfs.ext4 -L boot /dev/sda1
# mkfs.ext4 -L root /dev/nixos-vg/root
# mkswap -L swap /dev/nixos-vg/swap
```

### Mounting and activating the swap partition

We mount everything in `/mnt`, where we are going create the new system:

```
# mount /dev/nixos-vg/root /mnt
# mkdir /mnt/boot
# mount /dev/sda1 /mnt/boot
```

And we activate the encrypted swap:

```
# swapon /dev/nixos-vg/swap
```

### Basic configuration and instalation

As the first NixOS specific instalation step, type the following:
```
# nixos-generate-config --root /mnt
```
This creates two files containing a basic system configuration:
 - `etc/nixos/configuration.nix`
 - `etc/nixos/hardware-configuration.nix`

We have to change a few things in `configuration.nix` before proceeding,
so type the following:

```
# nano /mnt/etc/nixos/configuration.nix
```

Uncomment the line containing `# boot.loader.grub.device = "/dev/sda";`,
and make it look like this:

```
  boot.loader.grub.device = "/dev/sda";
  boot.initrd.luks.devices = [ { name = "lvm"; device = "/dev/sda2"; } ];
```

If you already have a ready to use `configuration.nix`, you can use it
instead of the default one. Just remember that you need the two lines above
to install grub and boot your system.

If you are unsure about it, you can use the default and change to the new
one after booting the new system, when you will be able to _rollback_ almost
anything that goes wrong.

Just keep in mind that you will need network access, so you may want
uncomment the line...

```
  network.wireless.enable = true;
```

or add...

```
  networking.networkmanager.enable = true;
```

to `configuration.nix`.

Cross your fingers...

```
# nixos-install
# reboot
```

:thumbsup::question:

Restoring the configuration
---------------------------

To rebuild this configuration, first you should edit and then link or copy
`nixos/configuration.nix` to `/etc/nixos/`.

Then you can run `sudo nixos-generate-config`, if you need to generate the
`/etc/nixos/hardware-configuration.nix` file, and `sudo nixos-rebuild switch`
to build the environment.

To restore user's dotfiles, you should link or copy each file in `homedir/`
to its corresponding location in `~/`.

[GNU Stow](https://www.gnu.org/software/stow/) can do this with a single
command if there are no conflicts: `stow --no-folding --target=$HOME homedir`

Customize Firefox installing
[Pentadactyl](http://5digits.org/pentadactyl/)
and
[this theme](https://userstyles.org/styles/117473/2015-solarized-dark-light-firefox-30-flat-theme)
with the
[Stylish](https://addons.mozilla.org/en-US/firefox/addon/stylish/?src=external-userstyleshome)
plugin.

Install [GreaseMonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)
with the [ViewTube](http://isebaro.com/viewtube/) script to avoid the
_Adobe Flash_ plugin, or you can just install it to get a reserved seat
in _IT Hell_, where people use _Internet Explorer_, are forced to write
_VBA_ code, and spend all eternity listening Steve Jobs _buzzword_-speeches
while reinstalling Windows daily.

### Installing Pentadactyl

To install Pentadactyl, first you'll have to set
`xpinstall.signatures.required` to false in `about:config`.

After that, you'll also have to clone the repository and compile it:

```
git clone https://github.com/5digits/dactyl
cd dactyl
make -C pentadactyl xpi
```

This will build the plugin in the `downloads` folder, ready to be manually
installed.

Alternativelly, you can install it with:

```
make -C pentadactyl install
```

I know it is a lot of work just to use a plugin, but it is definitelly
worthy.
