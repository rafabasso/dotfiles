{ config, pkgs, ... }:

# Testing with nixpkgs at commit '116c34a748599334555896c32ace86056607d6c4':
#   nix-channel --add
#     https://github.com/NixOS/nixpkgs/archive/116c34a748599334555896c32ace86056607d6c4.tar.gz
#     nixos
#   nix-channel --update
#   nixos-rebuild switch
# Problems:
#   - Cannot use Idris.

{
  imports = [
    /etc/nixos/hardware-configuration.nix
  ];

  nix = {
    useSandbox = true;

    # This removes the trailing 'nixpkgs' folder from NIX_PATH:
    nixPath = [ "nixpkgs=/nix/var/nix/profiles/per-user/root/channels/nixos"
                "nixos-config=/etc/nixos/configuration.nix" ];
  };

  boot = {
    loader.grub = {
      enable  = true;
      version = 2;
      device  = "/dev/sda";
    };
    initrd.luks.devices = [ { name = "lvm"; device = "/dev/sda2"; } ];
    kernelPackages = pkgs.linuxPackages_latest;
  };

  hardware = {
    bluetooth.enable = true;
    pulseaudio.enable = true;
    trackpoint = {
      enable       = true;
      emulateWheel = true;
      sensitivity  = 255;
      speed        = 255;
    };
  };

  systemd.services.power-tune = {
    description        = "Power Management tunings";
    serviceConfig.Type = "oneshot";
    wantedBy           = [ "multi-user.target" ];
    script = ''
      ${pkgs.powertop}/bin/powertop --auto-tune
    '';
  };

  system = {
    stateVersion = "18.03";
    copySystemConfiguration = true;
  };

  nixpkgs.config.allowUnfree = true;

  i18n.consoleUseXkbConfig = true;
  i18n.inputMethod = {
    enabled       = "fcitx";
    fcitx.engines = with pkgs.fcitx-engines; [ anthy ];
  };

  time.timeZone = "America/Sao_Paulo";

  fonts.fonts = with pkgs; [
    vistafonts
  ];

  networking.networkmanager.enable = true;

  virtualisation.virtualbox.host.enable = true;

# To enable keyboard brightness control.
  services.illum.enable = true;

  services.xserver = {
    enable             = true;
    layout             = "us";
    autoRepeatDelay    = 300;
    autoRepeatInterval =  20;
    xkbVariant         = "altgr-intl";
    xkbOptions         = "caps:swapescape";

    # Trying to fix resolution in KDE...
    #
    # ~/.Xresources also needs to set Xft.dpi to the
    # same value to allow GTK3 applications to work correctly.
    #
    dpi = 157;

    # Synaptics configuration
    synaptics = {
      enable          = true;
      twoFingerScroll = true;
    };

    # KDE Configuration
    displayManager.sddm.enable           = true;
    displayManager.sddm.autoLogin.enable = true;
    displayManager.sddm.autoLogin.user   = "user";
    desktopManager.plasma5.enable        = true;

    # Change the Print key to behave as Super_R.
    displayManager.sessionCommands = ''
      ${pkgs.xlibs.xmodmap}/bin/xmodmap -e "keycode 107 = Super_R"
    '';

    # XMonad configuration
    windowManager.xmonad.enable                 = true;
    windowManager.xmonad.enableContribAndExtras = true;
  };

  users.extraUsers.user = {
    isNormalUser    = true;
    home            = "/home/user";
    extraGroups     = [ "wheel" ];
    initialPassword = "user";
    shell           = pkgs.fish;
  };

  programs.fish.enable = true;

  programs.ssh = {
    startAgent = true;
    agentTimeout = "3h";
    knownHosts = [
      { hostNames = [ "github.com" ];
        publicKey = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==";
      }
    ];
  };

  environment.systemPackages = [
    pkgs.ntfs3g
  ];

}
