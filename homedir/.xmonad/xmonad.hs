import XMonad
import XMonad.Config.Kde
import XMonad.Layout.NoBorders

main = xmonad $ kde4Config
    { modMask            = mod4Mask
    , borderWidth        = 3
    , normalBorderColor  = "#eee8d5"
    , focusedBorderColor = "#859900"
    , focusFollowsMouse  = False
    , manageHook         = manageHook kde4Config <+> myManageHook
    , layoutHook         = smartBorders . layoutHook $ kde4Config
    }

myManageHook = composeAll . concat $
    [ [ className =? c --> doFloat | c <- myFloats      ]
    , [ title     =? t --> doFloat | t <- myOtherFloats ]
    ]
  where myFloats      = ["plasmashell", "VirtualBox"]
        myOtherFloats = ["alsamixer"]
