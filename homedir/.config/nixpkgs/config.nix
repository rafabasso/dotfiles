let hostPkgs   = import <nixpkgs> {};
    getNixpkgs = { rev, sha256 }:
      let owner   = "NixOS";
          repo    = "nixpkgs";
          nixpkgs = hostPkgs.fetchFromGitHub {
                      owner  = "NixOS";
                      repo   = "nixpkgs";
                      inherit rev sha256;
                    };
      in import nixpkgs { config = {}; };
    oldPkgs = getNixpkgs {
      rev    = "116c34a748599334555896c32ace86056607d6c4";
      sha256 = "1dh4n0kgkx9ja2w6shpzb6knbhlx9239l4r4ir96ybdvxh4p7xfg";
    };
    newPkgs = getNixpkgs {
      rev    = "db0da282c374e0cf7ec9309cbb36bf7b5b5d8e54";
      sha256 = "0pklxnbz9vr9gmcmz6kq267ix4jfbjy8sn3w0zpl6hiskbzgvavh";
    };
    pinned = { cowsay      = oldPkgs.cowsay;
               firefox-esr = oldPkgs.firefox-esr;
             };

in

{
  allowUnfree = true;

  packageOverrides = somePkgs: {
    all = with newPkgs; buildEnv {
      name = "all";
      paths = [

        # Living in the terminal.

        ## Shells

        konsole

        ## Customized Fish's greeting.

        figlet
        fortune
        pinned.cowsay  # New version gives Perl warning about locale.
        lolcat

        ## Basic utitities

        neovim
        xclip                   # Needed to copy and paste text.
        stow                    # Needed to link dotfiles.
        ncdu                    # Disk-usage analyzer.
        vifm                    # File manager with VI-style keybindings.
        file
        zip
        p7zip
        curl
        nix-repl

        # Programming

        ## General programming

        openssh                 # Needed to use SSH authentication in GitHub.
        git
        gnumake
        exercism

        ## Haskell programming

        stack
        hlint

        ## Idris programming

        idris


        # Hardware-related

        lm_sensors
        powertop


        # Graphical Interface

        ## Browsing

        qutebrowser
        pinned.firefox-esr  #  Updating make it unstable.
        chromium

        ## Miscellaneous

        calibre                 # E-Book reader
        zathura                 # PDF viewer
        ark
        gimp
        keepassx
        spectacle

      ];
    };
  };
}
