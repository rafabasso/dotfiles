" Appearance
"
set number
set relativenumber
set cursorline
set colorcolumn=80
set showcmd
"set showmatch
set list
set list listchars=trail:·,tab:▹\ ,nbsp:█
set termguicolors
"
" Solarized settings
colorscheme solarized8_light
"
" Italics
highlight Comment cterm=italic
"
" Indents and tabs
set expandtab
set softtabstop=2
set shiftwidth=2
"
" Miscellaneous
"set nomodeline
"set hidden
set wildmode=longest:full
