source ~/.config/nvim/config/plugins.vim
source ~/.config/nvim/config/personal.vim
source ~/.config/nvim/config/vim-hardtime.vim
source ~/.config/nvim/config/airline.vim
source ~/.config/nvim/config/easymotion.vim
