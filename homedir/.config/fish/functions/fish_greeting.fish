function fish_greeting
    begin
        figlet -f lean "NixOS"
        fortune -as | cowsay -f stimpy
    end | lolcat
end
