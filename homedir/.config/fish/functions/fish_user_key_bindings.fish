function fish_user_key_bindings

  fish_vi_key_bindings

  # Add a bind so that Control + G executes an autocomplete sugestion.
  bind -M insert \cg accept-autosuggestion execute

end
