[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=SolarizedLight
Font=Consolas,15,-1,5,50,0,0,0,0,0
LineSpacing=0
UseFontLineChararacters=false

[General]
Command=
Icon=utilities-terminal
Name=Default
Parent=FALLBACK/
ShowTerminalSizeHint=false
TerminalColumns=99
TerminalRows=99

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=2
ScrollBarPosition=2

[Terminal Features]
BidiRenderingEnabled=true
FlowControlEnabled=true
